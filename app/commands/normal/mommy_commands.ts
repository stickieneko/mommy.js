import { message_handler } from "../../handlers/message_handler";
import { mommy } from "../../mommy";
import { Message, RichEmbed } from "discord.js";
import { cache_data, cache_type } from "../../Types/cache_type";

export class mommy_commands {

    handler: message_handler;
    mommy: mommy;
    message: Message;

    constructor(handler: message_handler) {

        this.handler = handler;
        this.mommy = this.handler.mommy;
        this.message = this.handler.message;

        this.entry();
    }


    private entry(): void {

        let command = this.handler.cmd_text.split(" ");
        let result: cache_data | null = null;
        try {

            switch (command[0].toLocaleLowerCase()) {

                case "diaper":
                    result = this.diaper_response();
                    break;
            }
        } catch (e) {

            console.error(e);
        }

        if (result == null || result == undefined) return; // something want way wrong if we got here
        this.handler.send_message(this.handler.message.guild.id, this.handler.message.channel.id, this.embed(result));
    }

    private diaper_response(): cache_data {

        let cache = this.mommy.response_cache.get("mommy_diaper");
        if (cache == undefined) throw new Error("unknown cache `mommy_diaper`");

        let size = cache.data.length;
        let result = this.select_random(cache, size);
        result.response = `*Mommy.js hands you ${result.response}*`; // temp wrapper fix

        return result;
    }

    private select_random(cache: cache_type, size: number): cache_data {

        let rnd = Math.round(Math.random()) * size - 1;
        rnd = (rnd < 0 ? 0 : rnd); // i know how stupid this looks, but i cant remember if theres a better way
        return cache.data[rnd];
    }

    private embed(response: cache_data): RichEmbed {

        let embed = new RichEmbed();
        try {

            embed.setAuthor(this.message.member.displayName, this.message.author.avatarURL)
                .setColor(this.message.member.displayColor)
                .setThumbnail(this.mommy.discord.user.avatarURL)
                .setDescription(response.response)
                .setFooter("(#" + response.id + ")" + (response.credit_name != undefined ? " Credit: " + response.credit_name : ""));
        }
        catch (e) { }

        return embed;
    }
}
