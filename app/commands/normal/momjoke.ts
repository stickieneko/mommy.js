
import { mommy } from "../../mommy";
import { Message } from "discord.js";
import { message_handler } from "../../handlers/message_handler";
var https = require("https");

export class momjoke {


    handler: message_handler;
    mommy: mommy;
    message: Message;

    constructor(handler: message_handler) {

        this.handler = handler;
        this.mommy = this.handler.mommy;
        this.message = this.handler.message;

    }

    public entry(): void {

        var options = {
            hostname: 'icanhazdadjoke.com',
            port: 443,
            path: '/',
            method: 'GET',
            headers: {
                accept: 'application/json'
            }
        };

        try {

            https.get(options, this.result.bind(this));
        } catch (e) {

            this.message.reply("An unknown error has happened! I'm sorry cutie pie." + e);
        }
    }

    private result(res: any): void {

        let data = "";
        res.on('data', (d: any) => {

            data += d;
        });


        res.on('end', () => {
            try {

                this.message.reply(JSON.parse(data).joke);
            } catch (e) {

                console.log(e);
            }
        });
    }
}
