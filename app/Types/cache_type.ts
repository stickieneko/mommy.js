export class cache_type {
    
    data: Array<cache_data>
}

export class cache_data {
    id: string;
    response: string;
    credit_name: string;
    credit_link: string;
    catagories: Array<string>;
    booli: boolean;
    timeout: number;
    icon: string;
}