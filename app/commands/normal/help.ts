import { message_handler } from "../../handlers/message_handler";
import { mommy } from "../../mommy";
var os = require("os");

/**
 * @todo move all the text to a json file
 */
export class help {
    mommy: mommy;
    handler: message_handler;
    constructor(mommy: mommy, message: message_handler) {

        this.mommy = mommy;
        this.handler = message;
    }

    public async entry(): Promise<void> {

        let command = this.handler.message.content.substr(5).toLocaleLowerCase().trim();

        switch (command) {

            default:
                this.basic_response(); break;
        }
    }

    private basic_response(): void {

        let result = "";

        result += this.explain_response() + os.EOL;

        result += "Commands:"+ os.EOL;
        result += "?momjoke - let mommy tell you a joke!"+ os.EOL;

        result +=  os.EOL;
        result += "Join us at the home of Mommy.js!: <https://discord.gg/WH4hZt9>" + os.EOL;
        result += "Add Mommy.js to your own server!: <https://discordapp.com/oauth2/authorize?client_id=612140384901922847&scope=bot&permissions=2080767063>";

        this.handler.send_message(this.handler.message.guild.id, this.handler.message.channel.id, result);

    }

    private explain_response(): string {

        let result = "";
        result += "welcome to Mommy.js Help system!" + os.EOL;
        result += "This help system is currently underoerk" + os.EOL + os.EOL;
        result += "but for a basic intro for now, Mommy.js has 3 command prefix's:" + os.EOL;
        result += "? = for all normal commands" + os.EOL;
        result += "?? = for response commands that have a booli option" + os.EOL;
        result += "^ = for admin commands (use m^help for admin help) (not done yet)" + os.EOL;

        return result;

    }
    /**
     * 
     * @param basic
     * 
     * @todo if not basic
     */
    private momjoke_response(basic: boolean = true): string {

        let result = "";

        result += "```md" + os.EOL;

        result += "momjoke" + os.EOL;
        result += "===" + os.EOL;

        result += "useage: m?momjoke" + os.EOL;
        result += "has booli mode? No" + os.EOL + os.EOL;

        result += "Summons a mom joke" + os.EOL;

        result += "```" + os.EOL;

        return result;
    }

}