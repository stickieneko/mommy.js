import { mommy } from "../mommy";
import { Message, CollectorFilter, MessageReaction, MessageCollector, RichEmbed, TextChannel } from "discord.js";
import { help } from "../commands/normal/help";
import { momjoke } from "../commands/normal/momjoke";
import { mommy_commands } from "../commands/normal/mommy_commands";

/**
 * @TODO Error reporting in discord on fails
 */
export class message_handler {

    public mommy: mommy;
    public message: Message;
    private cmd_size: number = 0;
    public cmd_text: string = "";

    constructor(mommy: mommy, message: Message) {

        this.mommy = mommy;
        this.message = message;
    }

    //#region init
    public async init(): Promise<void> {

        // make sure isn't a bot
        if (this.message.author.bot) return;

        // now lets see if DM or guild
        if (await this.dm_check()) return;

        // handle possible command
        this.handle_possible_command();

        // do the member EXP
    }

    private async dm_check(): Promise<boolean> {

        if (this.message.guild === null) {

            this.message.reply(`I'm sorry sweetie, but mommy isn't able to accept DM's`);
            return true;
        }

        return false;
    }


    private async handle_possible_command() {

        if (this.message.content.startsWith("?")) {

            this.cmd_size += 1;
            await this.do_normal();
        } else if (this.message.content.startsWith("^")) {

            this.do_admin();
        }
    }
    //#endregion

    //#region to_clean
    private do_admin(): void {

        let command = this.message.content.substr(1).split(" ", 1)[0];

        switch (command.toLowerCase()) { }
    }

    private async do_normal(): Promise<void> {

        let command = this.message.content.substr(1).split(" ", 1)[0];
        let run;

        switch (command.toLowerCase()) {

            case "help":
                run = new help(this.mommy, this);
                await run.entry();
                break;

            case "momjoke":
                run = new momjoke(this);
                await run.entry();
                break;
            case "mommy":
                this.set_command_text(5);
                run = new mommy_commands(this);
                break;
        }
    }

    private set_command_text(cmd_size: number = 0): void {

        this.cmd_size += cmd_size;
        this.cmd_text = this.message.content.substr(this.cmd_size).trim();
    }

    public async send_message(guildID: string, channelID: string, message: string | RichEmbed): Promise<Message | undefined> {

        let channel = this.mommy.discord.guilds.find(guild => guild.id == guildID).channels.find(channel => channel.id == channelID) as TextChannel
        if (!channel) return;

        return await channel.send(message) as Message;
    }

    public async react_collector(filter: CollectorFilter, message: Message, callback: (react: MessageReaction) => Promise<void>): Promise<void> {

        const collector = message.createReactionCollector(filter, { time: 15000 });

        collector.on('collect', async (reaction: MessageReaction, reactionCollector: any) => {

            await callback(reaction);
            collector.stop("user");
        });

        collector.on('end', (c: any, reason: any) => {

            if (reason === "user") return;
            this.message.reply("sorry, timed out, you have to try again.");
        });
    }

    public async message_collector(filter: CollectorFilter, message: Message, callback: (react: Message) => Promise<void>) {

        const collector = new MessageCollector(message.channel, filter, { time: 15000 });

        collector.on('collect', async (response: any) => {

            await callback(response);
            collector.stop("user");

        });

        collector.on('end', (c: any, reason: any) => {

            if (reason === "user") return;
            this.message.reply("sorry, timed out, you have to try again.");
        });
    }
    //#endregion
}