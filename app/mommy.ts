import { Client, Message, TextChannel } from "discord.js";
import { configType } from "./Types/configType";
import { message_handler } from "./handlers/message_handler";
import { cache_type } from "./Types/cache_type";
import { mommy_commands } from "./commands/normal/mommy_commands";
//const fs = require("fs");

/**
 * mommy.ts is the base class of mommy.js
 */
export class mommy {

    public discord: Client;
    public config: configType | undefined;
    /**
     * the cache of all the json files for responses
     */
    public response_cache: Map<string, cache_type> = new Map();

    constructor(discord: Client) {

        this.discord = discord;
        this.config = this.open_config();
        this.open_response_files();

        // error check the config, if it fails, stop the boot
        if (this.config === undefined) {

            this.discord.destroy();
            return;
        }
    }

    private open_config(): configType | undefined {

        let config: configType | undefined = undefined;
        
        try {

            //let configFile = fs.readFileSync("./config/config.json");
            //config = JSON.parse(configFile) as configType;
        } catch {

            console.error("Config file not created");
        } finally {

            return config;
        }
    }

    /**
     * @todo to make this less hardcoded
     */
    private open_response_files(): void {
        
        //let diaper = fs.readFileSync("./data/diaper.json");
        //this.response_cache.set("mommy_diaper", JSON.parse(diaper) as cache_type);
    }

    public async message_handler(message: Message): Promise<void> {

        this.debug_console(`incoming message: "${message.guild.name}" from "${message.member.displayName}", in "${message.guild.channels.get(message.channel.id)?.name}"`);
        new message_handler(this, message).init();
    }

    public debug_console(output: string) {

        if( !this.config?.debug ) return;
        
        console.log(output);
    }
}