const Discord = require("discord.js");
const client = new Discord.Client();

const lib = require("./build/mommy");
const mommy = new lib.mommy(client);
client.on("message", message => {

  mommy.message_handler(message);
});

client.on("ready", () => {

  console.log(`Logged in as ${client.user.tag}!`);
  client.user.setPresence({ game: { name: `Use ?help for help!` }})
});


client.on("error", e => console.error(e));

client.login(mommy.config.botToken);

process.on('uncaughtException', function (err) {
  console.log('Caught exception: ' + err);
});
